<?php

namespace App\Tests\Entity;

use App\Entity\Measurement;
use PHPUnit\Framework\TestCase;

class MeasurementTest extends TestCase
{
    /**
     * @dataProvider dataGetFahrenheit
     */
    public function testGetFahrenheit($celsius, $expectedFahrenheit): void
    {
        $measurement = new Measurement();
        $measurement->setCelsius($celsius);
        print_r($measurement->getFahrenheit());
        $this->assertEquals($expectedFahrenheit, $measurement->getFahrenheit());
    }

    public function dataGetFahrenheit(): array
    {
        return [
            ['0', 32],
            ['-100', -148],
            ['100', 212],
            ['100.1', 212.18],
            ['45', 113],
            ['11', 51.8],
            ['90', 194],
            ['23.4', 74.12],
            ['87.1', 188.78],
            ['1', 33.8],
        ];
    }
}
