<?php

namespace App\Form;

use App\Entity\Location;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Range;
use Symfony\Component\Validator\Constraints\Type;

class LocationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('city', null, [
                'attr' => [
                    'placeholder' => 'Enter city name'
                ]
            ])
            ->add('country', ChoiceType::class, [
                'choices' => [
                    'Poland' => 'PL',
                    'Germany' => 'DE',
                    'France' => 'FR',
                    'Spain' => 'ES',
                    'Italy' => 'IT',
                    'United Kingdom' => 'GB',
                    'United States' => 'US',
                ]
            ])
            ->add('latitude', NumberType::class, [
                'constraints' => [
                    new NotBlank([
                        'message' => 'Please enter a latitude',
                    ]),
                    new Type([
                        'type' => 'numeric',
                        'message' => 'Latitude must be a number',
                        'groups' => ['create', 'edit']
                    ]),
                    new Range([
                        'min' => -90,
                        'max' => 90,
                        'notInRangeMessage' => 'Latitude must be between {{ min }} and {{ max }}',
                        'groups' => ['create', 'edit']
                    ])
                ]
            ])
            ->add('longitude', NumberType::class, [
                'constraints' => [
                    new NotBlank([
                        'message' => 'Please enter a longitude',
                    ]),
                    new Type([
                        'type' => 'numeric',
                        'message' => 'Longitude must be a number',
                        'groups' => ['create', 'edit']
                    ]),
                    new Range([
                        'min' => -180,
                        'max' => 180,
                        'notInRangeMessage' => 'Latitude must be between {{ min }} and {{ max }}',
                        'groups' => ['create', 'edit']
                    ])
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Location::class,
        ]);
    }
}
