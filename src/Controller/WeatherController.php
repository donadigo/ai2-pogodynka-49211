<?php

namespace App\Controller;

use App\Repository\LocationRepository;
use App\Service\WeatherUtil;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class WeatherController extends AbstractController
{
    #[Route('/weather/{city}', name: 'app_weather')]
    public function city(Request $request, string $city, WeatherUtil $util, LocationRepository $locationRepository): Response
    {
        $country = $request->query->get('country');

        $location = $locationRepository->findLocation($city, $country);
        if ($location == null) {
            return $this->render('base.html.twig', []);
        }

        if ($country == null) {
            $measurements = $util->getWeatherForLocation($location);
        } else {
            $measurements = $util->getWeatherForCountryAndCity($country, $city);
        }

        return $this->render('weather/city.html.twig', [
            'location' => $location,
            'measurements' => $measurements,
        ]);
    }
}
