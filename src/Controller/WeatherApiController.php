<?php

namespace App\Controller;

use App\Entity\Measurement;
use App\Service\WeatherUtil;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\MapQueryParameter;
use Symfony\Component\Routing\Annotation\Route;

class WeatherApiController extends AbstractController
{
    #[Route('/api/v1/weather', name: 'app_weather_api')]
    public function index(
        WeatherUtil $util,
        #[MapQueryParameter] string $city,
        #[MapQueryParameter] string $country,
        #[MapQueryParameter] string $format,
        #[MapQueryParameter] bool $twig = false): Response
    {
        $measurements = $util->getWeatherForCountryAndCity($country, $city);

        if ($twig) {
            if ($format == 'json') {
                return $this->render('weather_api/index.json.twig', [
                    'city' => $city,
                    'country' => $country,
                    'measurements' => $measurements,
                ]);
            } else if ($format == 'csv') {
                return $this->render('weather_api/index.csv.twig', [
                    'city' => $city,
                    'country' => $country,
                    'measurements' => $measurements,
                ]);
            } else {
                throw $this->createNotFoundException();
            }
        }

        if ($format == 'json') {
            return $this->json([
                'city' => $city,
                'country' => $country,
                'measurements' => array_map(fn(Measurement $m) => [
                    'date' => $m->getDate()->format('Y-m-d'),
                    'celsius' => $m->getCelsius(),
                    'fahrenheit' => $m->getFahrenheit(),
                ], $measurements),
            ]);
        } else if ($format == 'csv') {
            $content = implode("\n",
                array_map(
                    fn(Measurement $m) => sprintf("%s,%s,%s,%s,%s", $city, $country, $m->getDate()->format('Y-m-d'), $m->getCelsius(), $m->getFahrenheit()),
                    $measurements
                )
            );

            $resp = new Response($content, 200);
            $resp->headers->set('Content-Type', 'text/csv');
            return $resp;
        } else {
            throw $this->createNotFoundException();
        }
    }
}
